# Player Watermark plugin

A plugin that adds a watermark to the video player on your instance player.

Also a good demonstrator of how to add imports to your PeerTube plugin, and
how to modify the player.

![](http://lutim.cpy.re/MPv8mLIP.png)
